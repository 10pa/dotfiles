alias clean="paccache -rk1 -ruk0"
alias ytd="ytmdl"
alias e="micro"
alias v="sxiv"
alias update="sudo pacman -Syu"
alias purge="sudo pacman -Qdtq"
alias cpu='grep "cpu MHz" /proc/cpuinfo'
alias shinzuku="adb -s R39M30RN5WV shell sh /data/user_de/0/moe.shizuku.privileged.api/start.sh"
alias bixby="adb -s R39M30RN5WV shell sh /data/data/com.jamworks.bxactions/files/start_full_remap.sh"
alias maxpower="/usr/bin/nvidia-settings -a "[gpu:0]/GpuPowerMizerMode=1""
alias covid="curl 'https://corona-stats.online/KR?source=2&minimal=true'"
alias weather="curl wttr.in/seoul"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias aura='aura -c "c3 c7"'
export MICRO_TRUECOLOR=1
starship init fish | source
